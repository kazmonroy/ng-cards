import { HttpClient } from '@angular/common/http';
import { Injectable, inject } from '@angular/core';
import { BehaviorSubject, Observable, combineLatest, map } from 'rxjs';
import { HousingLocation } from './interface';

@Injectable({
  providedIn: 'root',
})
export class DataService {
  data = [
    {
      img: '1.jpg',
      title: 'Card 1',
      user: 'user_1',
      description:
        'Lorem ipsum dolor sit amet consectetur adipisicing elit. In, non.',
    },
    {
      img: '2.jpg',
      title: 'Card 2',
      user: 'user_2',
      description:
        'Lorem ipsum dolor sit amet consectetur adipisicing elit. In, non.',
    },
    {
      img: '3.jpg',
      title: 'Card 3',
      user: 'user_3',
      description:
        'Lorem ipsum dolor sit amet consectetur adipisicing elit. In, non.',
    },
  ];
}
