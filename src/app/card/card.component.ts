import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css'],
})
export class CardComponent {
  @Input() img = '';
  @Input() title!: string;
  @Input() user!: string;
  @Input() description!: string;
}
