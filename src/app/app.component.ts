import { Component, OnInit, inject } from '@angular/core';
import { DataService } from './data.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  dataService = inject(DataService);
  data = this.dataService.data;
}
